$(document).ready(function(){

	$.ajax({
		method: "GET",
		url: "https://jsonplaceholder.typicode.com/users",
		success: function(users){
			// console.log(users[1].name);
			var html = '';

			for(var i = 0; i < users.length; i++){
				var fullname = users[i].name;
				var username = users[i].username;
				var email = users[i].email;
				var city = users[i].address.city;

				html +=
					`<tr>
				      	<td>${fullname}</td>
				      	<td>${username}</td>
				      	<td>${email}</td>
				      	<td>${city}</td>
				      	<td>
				      		<button type="button" class="btn btn-success btn-sm btn-inner-success">Success</button>
				      		<button type="button" class="btn btn-warning btn-sm btn-inner-warning">Warning</button>
				      		<button type="button" class="btn btn-danger btn-sm btn-inner-danger">Disable</button>
				      		<button type="button" class="btn btn-dark btn-sm btn-inner-dark">Remove</button>
				      	</td>
				    </tr>`;
			}

			$('#tbody').html(html);
		}//success

	});//ajax


	$('.btn-makes-success').click(function(){
		$('table tbody tr').removeClass();
		$('table tbody tr').addClass('table-success');
	});

	$('.btn-makes-warning').click(function(){
		$('table tbody tr').removeClass();
		$('table tbody tr').addClass('table-warning');
	});

	$('.btn-makes-danger').click(function(){
		$('table tbody tr').removeClass();
		$('table tbody tr').addClass('table-danger');
	});

	$('.btn-makes-dark').click(function(){
		$('table tbody tr').remove();
	});//button 
	

	$(document).on('click', 'button.btn-inner-success', function(){
		$(this).parent().parent().removeClass();
		$(this).parent().parent().addClass('table-success');
	});

	$(document).on('click', 'button.btn-inner-warning', function(){
		$(this).parent().parent().removeClass();
		$(this).parent().parent().addClass('table-warning');
	});

	$(document).on('click', 'button.btn-inner-danger', function(){
		$(this).parent().parent().removeClass();
		$(this).parent().parent().addClass('table-danger');
	});

	$(document).on('click', 'button.btn-inner-dark', function(){
		$(this).parent().parent().remove();
	});//button inner


});