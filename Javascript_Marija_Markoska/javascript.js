//1del
document.getElementById("saveInfo").addEventListener('click', vnesiPodatoci);

function vnesiPodatoci(event){
	event.preventDefault();

	var fullname = document.getElementById('fullname').value;
	var email = document.getElementById('email').value;
	console.log(fullname);
	console.log(email);
	document.getElementById('text_fullname').innerHTML = fullname;
	document.getElementById('text_email').innerHTML = email;
	
	document.getElementById('fullname').value = '';
	document.getElementById('email').value = '';	
	document.getElementById('fullname').focus();
}
//2del
class Avtomobil{
	constructor(marka,model,godina,kilometri,poteklo){
		this.marka = marka;
		this.model = model;
		this.godina = godina;
		this.kilometri = kilometri;
		this.poteklo = poteklo;
	}
}

var avtomobili = [];

document.getElementById('zacuvajAvtomobil').addEventListener('click', zacuvajAvtomobil);
function zacuvajAvtomobil(event){
	event.preventDefault();

	var marka = document.getElementById('avtomobil_marka').value;
	var model = document.getElementById('avtomobil_model').value;
	var godina = document.getElementById('proizvodstvo').value;
	godina = parseInt(godina);
	var kilometri = document.getElementById('izminati_kilometri').value;
	kilometri = parseInt(kilometri);
	var poteklo = document.getElementById('zemja_na_poteklo').value;

	document.getElementById('tbody_sort').innerHTML += `<tr><td>${marka}</td><td>${model}</td><td>${godina}</td><td>${kilometri}</td><td>${poteklo}</td></tr>`;

	var new_obj = new Avtomobil(marka,model,godina,kilometri,poteklo);
	console.log(new_obj);

	avtomobili.push(new_obj);
	console.log(avtomobili);

	document.getElementById('avtomobil_marka').value = '';
	document.getElementById('avtomobil_model').value = '';
	document.getElementById('proizvodstvo').value = '';
	document.getElementById('izminati_kilometri').value = '';
	document.getElementById('zemja_na_poteklo').value = '';
	document.getElementById('avtomobil_marka').focus();
}
//2del a
document.getElementById('sort').addEventListener('click', sortiranje);
function sortiranje(){

	var izbran_sort = document.getElementById('sort_select').value;

	if(izbran_sort === 'proizodstvo_najstara'){
		avtomobili.sort(function(a,b){ return a.godina - b.godina});
	}else
	if(izbran_sort === 'proizodstvo_najnova'){
		avtomobili.sort(function(a,b){ return b.godina - a.godina});
	}else
	if(izbran_sort === 'kilometri_najmnogu'){
		avtomobili.sort(function(a,b){ return b.kilometri - a.kilometri});
	}else
	if(izbran_sort === 'kilometri_najmalku'){
		avtomobili.sort(function(a,b){ return a.kilometri - b.kilometri});
	}

	var html = '';
	for(var i = 0; i < avtomobili.length; i++){
		html += `<tr><td>${avtomobili[i].marka}</td><td>${avtomobili[i].model}</td><td>${avtomobili[i].godina}</td><td>${avtomobili[i].kilometri}</td><td>${avtomobili[i].poteklo}</td></tr>`;
	}
	document.getElementById('tbody_sort').innerHTML = html;
}

//2del b
document.getElementById('filter').addEventListener('click', isfiltriraj);
function isfiltriraj(){

	var izbrana_godina = document.getElementById('filter_select_km').value;
	var izbrano_poteklo = document.getElementById('filter_select_zemja').value;

	console.log(izbrano_poteklo);

	if(izbrana_godina === 'ponovi')
		{
			var new_avtomobili = avtomobili.filter(item => item.godina > 2000 && item.poteklo === izbrano_poteklo);
			console.log(new_avtomobili);
		}
	else
	if(izbrana_godina === 'postari')
		{
			var new_avtomobili = avtomobili.filter(item => item.godina < 2000 && item.poteklo === izbrano_poteklo);
			console.log(new_avtomobili);
		}

	var html = '';

	for(var i = 0; i < new_avtomobili.length; i++){

		html += `<tr><td>${new_avtomobili[i].marka}</td><td>${new_avtomobili[i].model}</td><td>${new_avtomobili[i].godina}</td><td>${new_avtomobili[i].kilometri}</td><td>${new_avtomobili[i].poteklo}</td></tr>`;
	}
	
	document.getElementById('tbody_filter').innerHTML = html;
}